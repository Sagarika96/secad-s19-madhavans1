<?php

	$mysqli = new mysqli('localhost','madhavans1','***','secad_s19');
		
		if($mysqli->connect_errno) {
			printf("Connection Failed: %s\n",$mysqli->connect_error);
			exit();
		}
function changepassword($username, $newpassword) {
		global $mysqli;
		//echo "user: $username pass: $newpassword";
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username=?;";
		//echo "DEBUG>prepared_sql=$prepared_sql\n";

  		if(!$stmt = $mysqli->prepare($prepared_sql)) 
  			return FALSE;
		
		$stmt->bind_param("ss",$newpassword,$username);
		
		if(!$stmt->execute()) 
			return FALSE;
		
		return TRUE;
		
	}

  	function addnewuser($username, $password,$emailid,$phoneno) {
		global 	$mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?,password(?),?,?);";
		echo "DEBUG:database.php>prepared_sql= $prepared_sql\n";

		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ssss", $username,$password,$emailid,$phoneno);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
  	}
  	function addnewpost($post){ 	
  		global 	$mysqli;
		$prepared_sql = "INSERT INTO posts(post) VALUES(?);";
		echo "post has been added!!";

		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("s",$post);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
  	}
  	function show_addpost()
	{
	global $mysqli;
	$prepared_sql ="select post, postid from posts";
	if(!$stmt = $mysqli->prepare($prepared_sql)) {echo "Cannot prepare"; die();}
	if(!$stmt->execute()) {echo "Cannot execute"; die();}
	$post = NULL; $postid = NULL;
	if(!$stmt-> bind_result($post, $postid)) echo "binding failed";
	while($stmt->fetch()){
	echo "post: " . htmlentities($post) . ". postid: " .
	htmlentities($postid) . "<br><a href='edit_post.php?postid=$postid'> edit </a><br>";
	}
	
	}

	function updatepost($postid,$post) {
	global $mysqli;
	   echo "Debug:Updatepost for Postid= $postid <br>";
	$prepared_sql = "UPDATE posts SET post=? WHERE postid=?;";

	if(!$stmt = $mysqli->prepare($prepared_sql))
	echo "Prepared statement error";
	$stmt->bind_param("si",htmlspecialchars($post),$postid);

	if(!$stmt->execute()) {echo "Execute Error"; return FALSE;}
	return TRUE;

	}
		// function new_comment($owner,$comments,$postid){
		// global $mysqli;
		// $prepared_sql = "INSERT into comment(owner,comments,postid) VALUES (?,?,?);";
		// if(!$stmt = $mysqli->prepare($prepared_sql))
		// echo "Prepared Statement Error";
		// $stmt->bind_param("ssi", htmlspecialchars($owner),htmlspecialchars($comments),$postid);
					 
		// if(!$stmt->execute()) {echo "Execute Error"; return FALSE;}
		// return TRUE;
		// }
function new_comment($owner,$comments,$postid){
	global 	$mysqli;
		$prepared_sql = "INSERT INTO comments(postid,owner,comments) VALUES(?,?,?);";
		echo "post has been added!!";

		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ssi",$owner,$comments,$postid);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
		}
	function display_comments($postid){
	   global $mysqli;
	   echo "Comments for Postid= $postid <br>";
	   $prepared_sql = "select owner, comments from comments where postid=?;";
	   if(!$stmt = $mysqli->prepare($prepared_sql))
		echo "Prepared Statement Error";
	   $stmt->bind_param('i', $postid);
	   if(!$stmt->execute()) echo "Execute failed ";
	   $title = NULL;
	   $content = NULL;
	   if(!$stmt->bind_result($owner,$comment)) echo "Binding failed ";
	   $num_rows = 0;
	   while($stmt->fetch()){ 
		echo "Comment title:" . htmlentities($owner) . "<br>";
		echo htmlentities($comment) . "<br>";
		$num_rows++;
	   } 
	   if($num_rows==0) echo "No comment for this post. Please post your comment";
	}


?>
