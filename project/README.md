1.INTRODUCTION:

A secure web application is being developed for Facebook and it is developed with all the programming principles to make sure that the code follows the secureness and the robust principles.
The facebook page is being named as ‘minibook’ where the functionality works similar to that of the facebook features and principles.
The functionality of the page is the users can login to a page if he/she is a existing users or else the user can freely register for and account to become an existing user.
The user logs into the page with the necessary credentials such as username and password. After logging in the user can post something the web page of their wish and the user can edit his/her own post and update it and can add comments to that particular post.
User cannot edit the posts of other users.
Admin is the only authorised user eligible to login and edit,view,comment,enable or disable the features of the existing users of the page.
Additionally the website has been developed with security features where the web page is being deployed in HTTPS domain. The passwords are hashed in the database where the password cannot be viewed by the DBA.
Instead of accessing the database with the root account a authenticated username and password has to be provided for the database and accessed.
The sql is accessed and displayed with the prepared statements.
The inputs and outputs have to be sanitized with html entities while validating the username and password to prevent sql injection and javascript attacks.
Access control option has to be provided for the users who are already registered or the one who is enabled in super category.
Session hijacking attack prevention is done when the attacker tries to access the session without user concern with the cookie id.
Csrf attack is prevented when the attacker tries to change the password or when the post is being edited without the user concern.


2.DESIGN:
Database:

The user database is designed with the relational schema where there is table such as users,posts,comment.
The users table consist of all the login informations and registered information.
Attributes : username,password,email,phone no,alternate email.
Posts table consist of post  and postid stores the posting informations.
Attributes: postid,post
Comment table has comment,owner referencing username in the users table and postid referencing the posts table.
Attributes: owner,comments,postid
User Interface:

The web interface has username and password area in form.php 
The registered user has inputs that accepts username,email,phone no,password.
The index page had the post text area to post something/logout/change password/post something/view post/comment for the post.
The post can be edited in the edit_post.php page.
The comments can be implemented in the comment.php page.
The posted information can be posted in post.php page.

Functionalities of the applications:

The regular users are the people who do not have any authority towards the other users but still the users can edit/comment their own post and view the other people posts.
The difference in functionality of super users is the user can access all the functions of the other users as well and enable/disable the user.



3.IMPLEMENTATION AND SECURITY ANALYSIS:

The program is made to be secure and robust to ensure that it follows all of the authentication mechanisms. The entire web page was deployed in the HTTPS platform such as sql injection, CSRF,XSS,session hijacking attack.

Database principles followed are the database is not accessed with root account a grant authentication is given and the passwords are hashed in the database.

The code is robust and defensive as the application can handle inappropriate inputs given by the user and it also prevents the abnormal termination of the program. This is maintained when the user will login/register/post/comment.Robustness is being handled by giving the error statements where the it will not lead to and defensive is executed by implementing the abnormal termination which is done as it throws invalidity. 

The code will defend against all kinds of security attacks such as sql injection,xss,csrf and session hijacking attack.sql injection attack is the attacker can login the web page with sql script and it can be prevented by adding htmlentities to the username and password and implementing the secure check login function and xss attack is prevented with the same implementation.session hijacking attack is prevented by setting the session’s browser to the http user agent and adding the secure,httponly and lifetime and set the cookie parameters.csrf attack is prevented by setting the token that is authenticated for only one session.

Super users can access all the information of all the users and regular users do not have authority to other users to make the changes hence the difference is existing.
