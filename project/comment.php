<?php  
  session_start();
  require 'database.php';
  $postid = $_REQUEST['postid'];
  if(!isset($postid)){
    echo "Bad Request";
    die();
  }
  function handle_new_comment($postid){
    $owner = $_POST['owner'];
    $nocsrftoken = $_POST["nocsrftoken"];
    $sessionnocsrftoken = $_SESSION["nocsrftoken"];
  if (isset($owner) and isset($comments) ){
	if(!isset($nocsrftoken) or ($nocsrftoken!=$sessionnocsrftoken)){
       echo "Cross-site request forgery is detected!";
       die();
       }
    if(new_comment($owner,$comments,$postid))
	echo "New comment added";
    else
	echo "Cannot add the comment";
    }
  }
  handle_new_comment($postid);
  
  display_comments($postid);
  $rand = bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;
?>
